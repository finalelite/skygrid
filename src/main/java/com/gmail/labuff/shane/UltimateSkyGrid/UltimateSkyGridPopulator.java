package com.gmail.labuff.shane.UltimateSkyGrid;

import org.bukkit.Axis;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Chest;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.block.data.Orientable;
import org.bukkit.block.data.type.Sapling;
import org.bukkit.entity.EntityType;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;
import java.util.Random;

public class UltimateSkyGridPopulator extends BlockPopulator {

    public void populate(World world, Random random, Chunk chunk) {
        int wH = world.getMaxHeight();
        for (int x = 0; x < 16; x += 4) {
            for (int y = 0; y < wH; y += 4) {
                for (int z = 0; z < 16; z += 4) {

                    Block blk = chunk.getBlock(x, y, z);
                    if (blk.getType() == Material.SAND && false) {
                        Random r = new Random();
                        int a = r.nextInt(10);

                        // 10% to put a cactus above a sand block
                        if (a < 1) {
                            /* old
                            blk.getRelative(BlockFace.UP).setTypeId(Material.CACTUS.getId(), false);
                            */

                            // set the block without physics
                            blk.getRelative(BlockFace.UP).setType(Material.CACTUS, false);
                        } else {
                            /* old
                            blk.getRelative(BlockFace.UP).setTypeId(Material.AIR.getId(), false);
                             */

                            // set the block without physics
                            blk.getRelative(BlockFace.UP).setType(Material.AIR, false);
                        }
                    } else if (blk.getType().isBlock() && blk.getType().name().contains("LOG")) {
                        Random rnd = new Random();
                        /* old
                        blk.setData((byte) rnd.nextInt(4));
                         */
                        Orientable data = (Orientable) blk.getBlockData();
                        // gets a random axis to place the log
                        data.setAxis(Axis.values()[rnd.nextInt(3)]);
                    }
                }
            }
        }
    }

}
