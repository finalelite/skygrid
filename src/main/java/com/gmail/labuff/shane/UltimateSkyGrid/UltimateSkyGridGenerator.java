package com.gmail.labuff.shane.UltimateSkyGrid;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class UltimateSkyGridGenerator extends ChunkGenerator {

    public int worldHeight;

    @Override
    public boolean canSpawn(World world, int x, int z) {
        return true;
    }

    @Override
    public ChunkGenerator.ChunkData generateChunkData(World world, Random random, int chunkX, int chunkZ, ChunkGenerator.BiomeGrid biomes) {
        worldHeight = UltimateSkyGrid.cHeight;

        ChunkData result = createChunkData(world);
        if (UltimateSkyGrid.genGlass) {
            for (int x = 0; x < 16; x++) {
                for (int y = 0; y < worldHeight; y++) {
                    for (int z = 0; z < 16; z++) {
                        if (x % 4 == 0 && y % 4 == 0 && z % 4 == 0) {
                            result.setBlock(x, y, z, getRandBlockMaterial(random));
                        } else {
                            if (y < (UltimateSkyGrid.cHeight - 3)) {
                                result.setBlock(x, y, z, Material.GLASS);
                            }
                        }
                    }
                }
            }
        } else {

            for (int x = 0; x < 16; x += 4) {
                for (int y = 0; y < UltimateSkyGrid.cHeight; y += 4) {
                    for (int z = 0; z < 16; z += 4) {
                        result.setBlock(x, y, z, getRandBlockMaterial(random));
                    }
                }
            }
        }
        return result;
    }

    @Override
    public Location getFixedSpawnLocation(World world, Random random) {
        return new Location(world, 0.5D, (double) worldHeight, 0.5D);
    }


    public List<BlockPopulator> getDefaultPopulators(World world) {

        return Arrays.asList(new BlockPopulator[]{new UltimateSkyGridPopulator()});
    }

    public Material getRandBlockMaterial(Random random) {

        Material item;
        int r;

        r = random.nextInt(10000);

        if (r < UltimateSkyGrid.cMythic) {
            item = UltimateSkyGrid.mNormMythic[random.nextInt(UltimateSkyGrid.mNormMythic.length)];
            return item;
        } else if (r < UltimateSkyGrid.cUnique) {
            item = UltimateSkyGrid.mNormUnique[random.nextInt(UltimateSkyGrid.mNormUnique.length)];
            return item;
        } else if (r < UltimateSkyGrid.cRare) {
            item = UltimateSkyGrid.mNormRare[random.nextInt(UltimateSkyGrid.mNormRare.length)];
            return item;
        } else if (r < UltimateSkyGrid.cUncommon) {
            item = UltimateSkyGrid.mNormUncommon[random.nextInt(UltimateSkyGrid.mNormUncommon.length)];
            return item;
        } else {
            item = UltimateSkyGrid.mNormAbundant[random.nextInt(UltimateSkyGrid.mNormAbundant.length)];
            return item;
        }
    }
}