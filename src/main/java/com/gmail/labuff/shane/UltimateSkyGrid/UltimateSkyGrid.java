package com.gmail.labuff.shane.UltimateSkyGrid;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class UltimateSkyGrid extends JavaPlugin {

    public static int cHeight = 128;
    public static int cMythic = 4;
    public static int cUnique = 181;
    public static int cRare = 1801;
    public static int cUncommon = 4001;
    public static boolean genGlass = false;

    public static Material[] mNormMythic = {Material.LAPIS_BLOCK, Material.GOLD_BLOCK, Material.IRON_BLOCK, Material.DIAMOND_BLOCK, Material.REDSTONE_BLOCK};
    public static Material[] mNormUnique = {Material.SAND, Material.GRAVEL, Material.DIAMOND_ORE, Material.SOUL_SAND, Material.GLOWSTONE, Material.MYCELIUM, Material.QUARTZ_BLOCK};
    public static Material[] mNormRare = {Material.GOLD_ORE, Material.LAPIS_ORE, Material.COBWEB, Material.GRAY_WOOL, Material.BRICKS, Material.OBSIDIAN, Material.REDSTONE_ORE, Material.PUMPKIN, Material.MELON, Material.NETHER_BRICKS, Material.NETHER_QUARTZ_ORE};
    public static Material[] mNormUncommon = {Material.IRON_ORE, Material.SANDSTONE, Material.MOSSY_STONE_BRICKS, Material.CLAY};
    public static Material[] mNormAbundant = {Material.STONE, Material.GRASS_BLOCK,
            Material.DIRT, Material.COAL_ORE, Material.OAK_LOG};

    @Override
    public void onEnable() {
        checkInvalidBlocks(mNormMythic);
        checkInvalidBlocks(mNormUnique);
        checkInvalidBlocks(mNormRare);
        checkInvalidBlocks(mNormUncommon);
        checkInvalidBlocks(mNormAbundant);

        getLogger().info("UltimateSkyGrid 1.13 SUPPORT by Clairton Santos enabled.");
    }

    public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
        return new UltimateSkyGridGenerator();
    }

    private static void checkInvalidBlocks(Material... materials) {
        List<Material> invalidMaterials = Arrays.stream(materials).filter(material -> !material.isBlock()).collect(Collectors.toList());

        invalidMaterials.forEach(material -> System.out.println("\n\n\nInvalid Block " + material.name() + ".\n\n"));
        if (!invalidMaterials.isEmpty())
            Bukkit.getServer().shutdown();
    }

}
